from django.shortcuts import render
from django.http import JsonResponse
import json
import requests

def home(request):
    return render(request, 'main/home.html')

def data(request):
    keyword = request.GET['q']
    url = 'https://www.googleapis.com/books/v1/volumes?q=' + keyword
    info = requests.get(url)
    data = json.loads(info.content)
    return JsonResponse(data, safe=False)